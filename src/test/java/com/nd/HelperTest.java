package com.nd;

import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.Assert.*;

/**
 * Created by Shishkov A.V. on 31.01.19.
 */
public class HelperTest {

	@Test(expected = UnknownHostException.class)
	public void parseWrongAddress() throws UnknownHostException {
		Helper helper = new Helper();
		helper.parseAddress("192.168.0.d");
	}

	@Test
	public void ipToLong() throws UnknownHostException {
		InetAddress address = InetAddress.getByName("0.0.0.1");
		Helper helper = new Helper();
		long value = helper.ipToLong(address);
		assertEquals("Неверный перевод из ip в long значения 0.0.0.1", value, 1L);
	}

	@Test
	public void longToIp() {
		long value = 3232235521L;
		String strValue = "192.168.0.1";
		Helper helper = new Helper();
		String result = helper.longToIp(value);
		assertEquals("Неверный перевод из long в ip", strValue, result);
	}
}