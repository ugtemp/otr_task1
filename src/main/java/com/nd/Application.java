package com.nd;

import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Created by Shishkov A.V. on 31.01.19.
 */
public class Application {
	public static void main(String[] args) {
		try(Scanner scanner = new Scanner(System.in)) {
			System.out.println("Введите первый ip-адрес:");
			String ip1 = scanner.nextLine();
			System.out.println("Введите второй ip-адрес:");
			String ip2 = scanner.nextLine();

			Helper helper = new Helper();

			try {
				helper.displayRange(ip1, ip2);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
	}
}
