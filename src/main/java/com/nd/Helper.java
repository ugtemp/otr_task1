package com.nd;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Shishkov A.V. on 31.01.19.
 */
public class Helper {
	public InetAddress parseAddress(String address) throws UnknownHostException {
		try {
			return InetAddress.getByName(address);
		} catch (UnknownHostException e) {
			throw new UnknownHostException("Ошибка ввода IP-адреса: " + address);
		}
	}

	public long ipToLong(InetAddress ip) {
		byte[] octets = ip.getAddress();
		long result = 0;
		for (byte octet : octets) {
			result <<= 8;
			result |= octet & 0xff;
		}
		return result;
	}

	public void displayRange(String ip1, String ip2) throws UnknownHostException {
		long address1 = ipToLong(parseAddress(ip1));
		long address2 = ipToLong(parseAddress(ip2));
		address1++;
		while (address1 < address2) {
			System.out.println(longToIp(address1++));
		}
	}

	public String longToIp(long value) {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < 4; i++) {
			String octet = Long.toString(0xff & value);
			result.insert(0, octet + ".");
			value >>= 8;
		}
		return result.delete(result.length() - 1, result.length()).toString();
	}
}
